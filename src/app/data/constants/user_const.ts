import { iCardUser } from "@shared/components/card/card-user/card-user-metadata";


export const USERS_DATA: iCardUser[] = [
    {
      id:1,
      avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
      name:'Erick Briones',
      age:31,
      description:'Soy Ingeniero'
    },
    {
      id:2,
      avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
      name:'Luis Cevallos',
      age:40,
      description:'Soy Ingeniero',
      work:'Desarrollador Web'
    },
    {
      id:3,
      avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
      name:'Ruth Hernandez',
      age:25,
      description:'Soy Ingeniero'
    },
    {
      id:4,
      avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
      name:'Miguel Briones',
      age:22,
      description:'Soy Ingeniero'
    },
    {
      id:5,
      avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
      name:'Erick Briones',
      age:31,
      description:'Soy Ingeniero'
    },
    {
      id:6,
      avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
      name:'Luis Cevallos',
      age:40,
      description:'Soy Ingeniero'
    },
    {
      id:7,
      avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
      name:'Ruth Hernandez',
      age:25,
      description:'Soy Ingeniero'
    },
    {
      id:8,
      avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
      name:'Miguel Briones',
      age:22,
      description:'Soy Ingeniero'
    }
]