import {HttpClient, HttpErrorResponse } from '@angular/common/http';
import {environment} from 'environments/environment';
import { of } from 'rxjs';

export class ApiClass{
    public url: string = environment.url;
    public isProduction = environment.production
    constructor(protected http: HttpClient){
        
    }

    error(error:HttpErrorResponse){
        let errorMessage = "";
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
            console.log('PRIMER ERROR');
            
        }else{
            errorMessage =  `Error code: ${error.status} \nMessage:_${error.message}`
            console.log('SEGUNDO ERROR');
            
        }
        return of({error:true, msg:errorMessage, data:null});
    }
}