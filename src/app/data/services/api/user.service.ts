import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiClass } from '@data/schema/ApiClass.class';
import { iCardUser } from '@shared/components/card/card-user/card-user-metadata';
import { Observable, of } from 'rxjs';
import { catchError, map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserService extends ApiClass {

  constructor( protected override http: HttpClient) {
    super(http);
}

  /**
   * 
   * GET ALL USERS FROM API
   */
  getAllUsers(): Observable<{error:boolean, msg:string, data:iCardUser[]}>{
    //const response = {error:false, msg:'', data:null};
    const response = {
      error:false, 
      msg:'', 
      data:[] as iCardUser[]
    };
    return this.http.get<iCardUser[]>(this.url + 'users')
    .pipe(
      map(r => {
      response.data = r;
      return response;
    }), 
    //catchError(this.error)
    catchError(()=>of(response)));
  }

  /**
   * GET ONE USER BY ID
   * @param id number
   */

  getUserById(id: number): Observable<{error:boolean, msg:string, data:iCardUser}>{
    const response = {
      error:false, 
      msg:'', 
      data:{} as iCardUser
    };
    return this.http.get<iCardUser>(this.url + 'users/' + id)
    .pipe(
      map(r =>{
        response.data = r;
        return response;
      }),catchError(() => of(response))
      )
  }


}
