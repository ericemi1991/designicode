import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  src: string = "https://pbs.twimg.com/profile_images/1106321037325754370/DFN9QZve_400x400.png"

  constructor() { }

  ngOnInit(): void {
  }

}
