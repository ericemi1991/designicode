import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { USERS_DATA } from '@data/constants/user_const';
import { UserService } from '@data/services/api/user.service';
import { iCardUser } from '@shared/components/card/card-user/card-user-metadata';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  title:string = 'DETALLE DE UISUARIOS';
  public users: iCardUser[] = []//USERS_DATA;
  //public currentUser: iCardUser;
  public currentUser: any;
  id: number;

  

  constructor(private route: ActivatedRoute, private userService: UserService) { 

   //this.id =  +this.route.snapshot.params['id'];
   //no funciono la linea de arriba, se corrigio en la siguiente linea  
   this.id =  +this.route.snapshot.params['id'];
   //this.currentUser = this.users.find(user => user.id === this.id);
   //this.currentUser = this.users.find(user => user.id === this.id);
  }

  ngOnInit(): void {
      this.userService.getUserById(this.id).subscribe(r => {
        if (!r.error) {
          this.currentUser = r.data;
        }
      });
  }

}
