import { Component, OnInit } from '@angular/core';
import { USERS_DATA } from '@data/constants/user_const';
import { UserService } from '@data/services/api/user.service';
import { iCardUser } from '@shared/components/card/card-user/card-user-metadata';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  title:string = 'LISTA DE UISUARIOS';

  //objeto
  /*public user: iCardUser = {
    avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
    name:'Erick Briones',
    age:31,
    description:'Soy Ingeniero'
  }*/

  //array

  public users: iCardUser[] = [];// = USERS_DATA

  constructor(private userServices: UserService) {
    this.userServices.getAllUsers().subscribe(r=> {
      //constconsole.log(r.data);
      if (!r.error) {
        this.users = r.data
      }
    })
   }

  ngOnInit(): void {
  }

}
