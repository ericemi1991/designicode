export interface iCardUser{
    id:number;
    avatar:string;
    name:string;
    age:number;
    description:string;
    work?:string

}