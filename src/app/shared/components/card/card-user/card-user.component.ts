import { Component, Input, OnInit } from '@angular/core';
import { iCardUser } from './card-user-metadata';

@Component({
  selector: 'app-card-user',
  templateUrl: './card-user.component.html',
  styleUrls: ['./card-user.component.css']
})
export class CardUserComponent implements OnInit {

  @Input() data: iCardUser = 
    {
      id:0,
      avatar:'', 
      name:'', 
      age:0, 
      description:'',
      work:''
    };

  /*public data: iCardUser = {
    avatar:'https://www.getbillage.com/files/user/avatar/_usuario.png',
    name:'Erick',
    age:31,
    description:'Soy Ingeniero'
  }*/

  constructor() { }

  ngOnInit(): void {
  }

}
